# Orchestration - Clouds

This will enable the packing/deploying to client 'clouds'.


## Pre-commit Hooks
### Windows
**Dependencies**
1. `pip install pre-commit`
1. `pip install checkov`
1. `choco install tflint`
1. `choco install terraform-docs`
1. `choco install tfsec`


## Install your pre-commit hooks
1. `pre-commit install`

## Running It!
1. `pre-commit run -a`