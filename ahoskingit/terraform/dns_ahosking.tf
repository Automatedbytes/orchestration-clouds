#Terraform and its management of DNS in Cloudflare

resource "cloudflare_zone" "ahosking_com" {
  zone = "ahosking.com"
  plan = "free"
}

resource "cloudflare_record" "ahosking" {
  zone_id = cloudflare_zone.ahosking_com.id
  name    = "ahosking.com"
  type    = "A"
  ttl     = "1"
  value   = var.site5_ip
}

resource "cloudflare_record" "ahosking_wildcard" {
  zone_id = cloudflare_zone.ahosking_com.id
  name    = "*.ahosking.com"
  type    = "A"
  ttl     = "1"
  value   = var.home_ip
  proxied = true
}

resource "cloudflare_record" "ahosking_www" {
  zone_id = cloudflare_zone.ahosking_com.id
  name    = "www.ahosking.com"
  type    = "CNAME"
  ttl     = "1"
  value   = cloudflare_record.ahosking.name
  proxied = "true"
}

resource "cloudflare_record" "ahosking_home" {
  zone_id = cloudflare_zone.ahosking_com.id
  name    = "home"
  type    = "A"
  ttl     = "1"
  value   = var.home_ip
  proxied = true
}

resource "cloudflare_record" "ahosking_cnames" {
  for_each = toset(var.ahosking_com_cnames)
  zone_id  = cloudflare_zone.ahosking_com.id
  name     = each.key
  type     = "CNAME"
  ttl      = "1"
  value    = join(".", [cloudflare_record.ahosking_home.name, cloudflare_zone.ahosking_com.zone])
  proxied  = true
}

resource "cloudflare_record" "ahosking_food" {
  zone_id = cloudflare_zone.ahosking_com.id
  name    = "food"
  type    = "CNAME"
  ttl     = "1"
  proxied = true
  value   = join(".", [cloudflare_record.ahosking_home.name, cloudflare_zone.ahosking_com.zone])
}

resource "cloudflare_record" "ahosking_recipes" {
  zone_id = cloudflare_zone.ahosking_com.id
  name    = "recipes"
  type    = "CNAME"
  ttl     = "1"
  proxied = true
  value   = join(".", [cloudflare_record.ahosking_home.name, cloudflare_zone.ahosking_com.zone])
}

resource "cloudflare_record" "ahosking_paperless" {
  zone_id = cloudflare_zone.ahosking_com.id
  name    = "paperless"
  type    = "CNAME"
  ttl     = "1"
  proxied = true
  value   = join(".", [cloudflare_record.ahosking_home.name, cloudflare_zone.ahosking_com.zone])
}

# email MX value for DNS
resource "cloudflare_record" "ahosking_mx" {
  zone_id  = cloudflare_zone.ahosking_com.id
  name     = cloudflare_zone.ahosking_com.zone
  type     = "MX"
  ttl      = "60"
  value    = "aspmx.l.google.com"
  priority = 1
}
resource "cloudflare_record" "ahosking_mx1" {
  zone_id  = cloudflare_zone.ahosking_com.id
  name     = cloudflare_zone.ahosking_com.zone
  type     = "MX"
  ttl      = "60"
  value    = "alt1.aspmx.l.google.com"
  priority = 5
}
resource "cloudflare_record" "ahosking_mx2" {
  zone_id  = cloudflare_zone.ahosking_com.id
  name     = cloudflare_zone.ahosking_com.zone
  type     = "MX"
  ttl      = "60"
  value    = "alt2.aspmx.l.google.com"
  priority = 5
}
resource "cloudflare_record" "ahosking_mx3" {
  zone_id  = cloudflare_zone.ahosking_com.id
  name     = cloudflare_zone.ahosking_com.zone
  type     = "MX"
  ttl      = "60"
  value    = "alt3.aspmx.l.google.com"
  priority = 10
}
resource "cloudflare_record" "ahosking_mx4" {
  zone_id  = cloudflare_zone.ahosking_com.id
  name     = cloudflare_zone.ahosking_com.zone
  type     = "MX"
  ttl      = "60"
  value    = "alt4.aspmx.l.google.com"
  priority = 10
}
resource "cloudflare_record" "ahosking_txt" {
  zone_id = cloudflare_zone.ahosking_com.id
  name    = cloudflare_zone.ahosking_com.zone
  type    = "TXT"
  ttl     = 60
  value   = "google-site-verification=8-QKnBQElI58gnygDWcCzvDGRh31c_bFmNMaSd0fCwE"
}
resource "cloudflare_record" "ahosking_spf" {
  zone_id = cloudflare_zone.ahosking_com.id
  name = "@"
  type = "TXT"
  ttl = 60
  value = "v=spf1 include:_spf.google.com ~all"
}
resource "cloudflare_record" "ahosking_dkim" {
  zone_id = cloudflare_zone.ahosking_com.id
  name = "google._domainkey"
  type = "TXT"
  ttl = 60
  value = "v=DKIM1; k=rsa; p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAyRmf8DwPqa19cGfOr4yDtu0l4JBLNOvnRGTHh4k0AIiqTXeGnclAWs6Y+d0izJu2GIdwJ6X67TUnx51DVV67SOfKjD8PvlIlF9KCJiqLeuVr0wsJ9eRCiaQqpch9ocP+908FrA86faSdbXjq8PGz7phrjDqwaW+UEZcWUApXhABKanPSdP4hLV7/M3yz5/Wo5sXaNLcc0DZRL0+/7ifssIL/mbdFczh3ngOyqVeYgzYYj23fO2E/+A5ED9AIyXpUI2reVonnU3qI/BpaLp7xnAh3duhofdGKbjGD6P9G5mA6UPTWHqAdIhvJsgwtC5xmxtTXb7ACObRzYrSGynKa2wIDAQAB"
}
resource "cloudflare_record" "ahosking_dmarc" {
  zone_id = cloudflare_zone.ahosking_com.id
  name = "_dmarc.ahosking.com"
  type = "TXT"
  ttl = 300
  value = "v=DMARC1; p=quarantine; rua=mailto:dmarc-reports@ahosking.com; ruf=mailto:dmarc-failures@ahosking.com; pct=100; sp=reject; aspf=s;"
}