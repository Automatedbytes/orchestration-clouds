##Terraform and its management of DNS in Cloudflare
#### automatedbytes.com
resource "cloudflare_zone" "automatedbytes_com" {
  zone = "automatedbytes.com"
}

resource "cloudflare_record" "automatedbytes" {
  zone_id = cloudflare_zone.automatedbytes_com.id
  name    = "automatedbytes.com"
  type    = "A"
  ttl     = "1"
  proxied = true
  value   = var.home_ip
}

resource "cloudflare_record" "assets_automatedbytes" {
  zone_id = cloudflare_zone.automatedbytes_com.id
  name    = "assets"
  type    = "A"
  proxied = false
  value   = var.do_assets
}

resource "cloudflare_record" "assets_api_automatedbytes" {
  zone_id = cloudflare_zone.automatedbytes_com.id
  name    = "assets-api"
  type    = "A"
  proxied = false
  value   = var.do_assets
}

resource "cloudflare_record" "assets_mesh_automatedbytes" {
  zone_id = cloudflare_zone.automatedbytes_com.id
  name    = "assets-mesh"
  type    = "A"
  proxied = false
  value   = var.do_assets
}

resource "cloudflare_record" "support_automatedbytes" {
  zone_id = cloudflare_zone.automatedbytes_com.id
  name    = "support"
  type    = "A"
  proxied = true
  value   = var.home_ip
}

resource "cloudflare_record" "itmanagement_automatedbytes" {
  zone_id = cloudflare_zone.automatedbytes_com.id
  name    = "itmanagement"
  type    = "CNAME"
  ttl     = "1"
  proxied = true
  value   = join(".", [cloudflare_record.support_automatedbytes.name, cloudflare_zone.automatedbytes_com.zone])
}

