resource "cloudflare_access_group" "family" {
  account_id = var.CLOUDFLARE_ACCOUNT_ID
  name       = "family group"

  include {
    email_domain = ["@ahosking.com"]
  }
}

resource "cloudflare_access_application" "influx" {
  account_id       = var.CLOUDFLARE_ACCOUNT_ID
  name             = "influx"
  domain           = "influx.ahoskingit.com"
  type             = "self_hosted"
  session_duration = "168h"
  logo_url         = "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fcommons%2Fthumb%2Fc%2Fc6%2FInfluxdb_logo.svg%2F1920px-Influxdb_logo.svg"
  allowed_idps = [
    "f27ba272-b676-4ab0-b560-e4ea52b19f18", #TODO: Remove Manual Entry
    "ba68cb89-c20b-49ab-8aea-26b3af318196", #TODO: Remove Manual Entry
  ]
}

resource "cloudflare_access_application" "money" {
  account_id       = var.CLOUDFLARE_ACCOUNT_ID
  name             = "money"
  domain           = "money.ahosking.com"
  type             = "self_hosted"
  session_duration = "336h"
  allowed_idps = [
    "f27ba272-b676-4ab0-b560-e4ea52b19f18", #TODO: Remove Manual Entry
  ]
  http_only_cookie_attribute = true
}

resource "cloudflare_access_policy" "money" {
  application_id = cloudflare_access_application.money.id
  zone_id        = cloudflare_zone.ahosking_com.id
  name           = "family"
  precedence     = "2"
  decision       = "allow"

  include {
    email_domain = ["@ahosking.com"]
  }
  require {
    email_domain = ["@ahosking.com"]
  }
}

resource "cloudflare_access_application" "movies" {
  account_id       = var.CLOUDFLARE_ACCOUNT_ID
  name             = "movies"
  domain           = "movies.ahosking.com"
  type             = "self_hosted"
  session_duration = "336h"
  allowed_idps = [
    "f27ba272-b676-4ab0-b560-e4ea52b19f18", #TODO: Remove Manual Entry
  ]
}

resource "cloudflare_access_application" "tv" {
  account_id       = var.CLOUDFLARE_ACCOUNT_ID
  name             = "tv"
  domain           = "tv.ahosking.com"
  type             = "self_hosted"
  session_duration = "336h"
  allowed_idps = [
    "f27ba272-b676-4ab0-b560-e4ea52b19f18", #TODO: Remove Manual Entry
  ]
}

resource "cloudflare_access_application" "cycles" {
  account_id       = var.CLOUDFLARE_ACCOUNT_ID
  name             = "cycles"
  domain           = "cycles.ahosking.com"
  type             = "self_hosted"
  session_duration = "336h"
  allowed_idps = [
    "f27ba272-b676-4ab0-b560-e4ea52b19f18", #TODO: Remove Manual Entry
  ]

  http_only_cookie_attribute = true
}

resource "cloudflare_access_policy" "cycles" {
  application_id = cloudflare_access_application.cycles.id
  zone_id        = cloudflare_zone.ahosking_com.id
  name           = "family"
  precedence     = "1"
  decision       = "allow"
  include {
    email_domain = ["@ahosking.com"]
  }
  require {
    email_domain = ["@ahosking.com"]
  }
}
