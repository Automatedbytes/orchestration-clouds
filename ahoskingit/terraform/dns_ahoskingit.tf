#Terraform and its management of DNS in Cloudflare
resource "cloudflare_zone" "ahoskingit_com" {
  zone = "ahoskingit.com"
  plan = "free"
}

resource "cloudflare_record" "ahoskingit" {
  zone_id = cloudflare_zone.ahoskingit_com.id
  name    = "ahoskingit.com"
  type    = "A"
  ttl     = "1"
  value   = var.site5_ip
  proxied = "true"
}

resource "cloudflare_record" "ahoskingit_www" {
  zone_id = cloudflare_zone.ahoskingit_com.id
  name    = "www"
  type    = "CNAME"
  value   = cloudflare_record.ahoskingit.name
  proxied = "true"
}

resource "cloudflare_record" "ahoskingit_influx" {
  zone_id = cloudflare_zone.ahoskingit_com.id
  name    = "influx"
  type    = "CNAME"
  value   = join(".", [cloudflare_record.home_ahoskingit.name, cloudflare_zone.ahoskingit_com.zone])
  proxied = "true"
}

#resource "cloudflare_record" "ahoskingit_wildcard" {
#    domain = "ahoskingit.com"
#    name = "*"
#    type = "A"
#    value = var.home_ip
#    proxied = "true" #A records cannot be proxied if wildcard
#}

resource "cloudflare_record" "ahoskingit_plex" {
  zone_id = cloudflare_zone.ahoskingit_com.id
  name    = "plex.ahoskingit.com"
  type    = "CNAME"
  value   = join(".", [cloudflare_record.home_ahoskingit.name, cloudflare_zone.ahoskingit_com.zone])
  proxied = "true"
}

resource "cloudflare_record" "ahoskingit_recipes" {
  zone_id = cloudflare_zone.ahoskingit_com.id
  name    = "recipes.ahoskingit.com"
  type    = "CNAME"
  value   = join(".", [cloudflare_record.home_ahoskingit.name, cloudflare_zone.ahoskingit_com.zone])
  proxied = "true"
}

resource "cloudflare_record" "ahoskingit_food" {
  zone_id = cloudflare_zone.ahoskingit_com.id
  name    = "food.ahoskingit.com"
  type    = "CNAME"
  value   = join(".", [cloudflare_record.home_ahoskingit.name, cloudflare_zone.ahoskingit_com.zone])
  proxied = "true"
}

resource "cloudflare_record" "ahoskingit_jumbledev" {
  zone_id = cloudflare_zone.ahoskingit_com.id
  name    = "jumbledev.ahoskingit.com"
  type    = "CNAME"
  value   = join(".", [cloudflare_record.home_ahoskingit.name, cloudflare_zone.ahoskingit_com.zone])
  proxied = "true"
}

resource "cloudflare_record" "home_ahoskingit" {
  zone_id = cloudflare_zone.ahoskingit_com.id
  name    = "home"
  type    = "A"
  value   = var.home_ip
  proxied = "true"
}
### Home Subdomains ###
resource "cloudflare_record" "smart_home_ahoskingit" {
  zone_id = cloudflare_zone.ahoskingit_com.id
  name    = "smart"
  type    = "CNAME"
  value   = join(".", [cloudflare_record.home_ahoskingit.name, cloudflare_zone.ahoskingit_com.zone])
  proxied = "true"
}

resource "cloudflare_record" "temps_home_ahoskingit" {
  zone_id = cloudflare_zone.ahoskingit_com.id
  name    = "temperatures"
  type    = "CNAME"
  value   = join(".", [cloudflare_record.home_ahoskingit.name, cloudflare_zone.ahoskingit_com.zone])
  # value = var.home_ip
  proxied = "true"
}
### END Home Subdomains ###

resource "cloudflare_record" "kfa_ahoskingit" {
  zone_id = cloudflare_zone.ahoskingit_com.id
  name    = "kfa"
  type    = "A"
  value   = var.kfa_ip
}

resource "cloudflare_record" "lab_ahoskingit" {
  zone_id = cloudflare_zone.ahoskingit_com.id
  name    = "lab"
  type    = "CNAME"
  proxied = true
  value   = join(".", [cloudflare_record.kfa_ahoskingit.name, cloudflare_zone.ahoskingit_com.zone])
}

resource "cloudflare_record" "logs_ahoskingit" {
  zone_id = cloudflare_zone.ahoskingit_com.id
  name    = "logs"
  type    = "A"
  proxied = true
  value   = var.home_ip
}

resource "cloudflare_record" "inventory_ahoskingit" {
  zone_id = cloudflare_zone.ahoskingit_com.id
  name    = "inventory"
  type    = "CNAME"
  proxied = true
  value   = join(".", [cloudflare_record.home_ahoskingit.name, cloudflare_zone.ahoskingit_com.zone])
}

# resource "cloudflare_record" "requests_plex_ahoskingit" {
#     zone_id = cloudflare_zone.ahoskingit_com
#     name = "requests.plex"
#     type = "CNAME"
#     proxied = true
#     value = "cloudflare_record.home_ahoskingit.name"
# }

resource "cloudflare_record" "ns1_ahoskingit" {
  zone_id = cloudflare_zone.ahoskingit_com.id
  name    = "ns1"
  type    = "CNAME"
  proxied = true
  value   = join(".", [cloudflare_record.home_ahoskingit.name, cloudflare_zone.ahoskingit_com.zone])
}

resource "cloudflare_record" "docs_ahoskingit" {
  zone_id = cloudflare_zone.ahoskingit_com.id
  name    = "docs"
  type    = "CNAME"
  proxied = true
  value   = join(".", [cloudflare_record.home_ahoskingit.name, cloudflare_zone.ahoskingit_com.zone])
}

resource "cloudflare_record" "cloud_ahoskingit" {
  zone_id = cloudflare_zone.ahoskingit_com.id
  name    = "cloud"
  type    = "CNAME"
  proxied = true
  value   = join(".", [cloudflare_record.home_ahoskingit.name, cloudflare_zone.ahoskingit_com.zone])
}

resource "cloudflare_record" "goals_ahoskingit" {
  zone_id = cloudflare_zone.ahoskingit_com.id
  name    = "goals"
  type    = "CNAME"
  proxied = true
  value   = join(".", [cloudflare_record.home_ahoskingit.name, cloudflare_zone.ahoskingit_com.zone])
}

resource "cloudflare_record" "office_ahoskingit" {
  zone_id = cloudflare_zone.ahoskingit_com.id
  name    = "office"
  type    = "CNAME"
  proxied = true
  value   = join(".", [cloudflare_record.home_ahoskingit.name, cloudflare_zone.ahoskingit_com.zone])
}

resource "cloudflare_record" "support_ahoskingit" {
  zone_id = cloudflare_zone.ahoskingit_com.id
  name    = "support"
  type    = "CNAME"
  proxied = true
  value   = join(".", [cloudflare_record.home_ahoskingit.name, cloudflare_zone.ahoskingit_com.zone])
}

resource "cloudflare_record" "git_ahoskingit" {
  zone_id = cloudflare_zone.ahoskingit_com.id
  name    = "git"
  type    = "CNAME"
  value   = join(".", [cloudflare_record.home_ahoskingit.name, cloudflare_zone.ahoskingit_com.zone])
  proxied = "true"
}

resource "cloudflare_record" "gitlab_ahoskingit" {
  zone_id = cloudflare_zone.ahoskingit_com.id
  name    = "gitlab"
  type    = "CNAME"
  proxied = true
  value   = join(".", [cloudflare_record.home_ahoskingit.name, cloudflare_zone.ahoskingit_com.zone])
}

resource "cloudflare_record" "monitors_ahoskingit" {
  zone_id = cloudflare_zone.ahoskingit_com.id
  name    = "monitors"
  type    = "CNAME"
  value   = join(".", [cloudflare_record.home_ahoskingit.name, cloudflare_zone.ahoskingit_com.zone])
  proxied = true
}

# email MX records for DNS
resource "cloudflare_record" "ahoskingit_com_mx" {
  zone_id  = cloudflare_zone.ahoskingit_com.id
  name     = cloudflare_zone.ahoskingit_com.zone
  type     = "MX"
  value    = "aspmx.l.google.com"
  priority = 1
}
resource "cloudflare_record" "ahoskingit_com_mx1" {
  zone_id  = cloudflare_zone.ahoskingit_com.id
  name     = cloudflare_zone.ahoskingit_com.zone
  type     = "MX"
  value    = "alt1.aspmx.l.google.com"
  priority = 5
}
resource "cloudflare_record" "ahoskingit_com_mx2" {
  zone_id  = cloudflare_zone.ahoskingit_com.id
  name     = cloudflare_zone.ahoskingit_com.zone
  type     = "MX"
  value    = "alt2.aspmx.l.google.com"
  priority = 5
}
resource "cloudflare_record" "ahoskingit_com_mx3" {
  zone_id  = cloudflare_zone.ahoskingit_com.id
  name     = cloudflare_zone.ahoskingit_com.zone
  type     = "MX"
  value    = "alt3.aspmx.l.google.com"
  priority = 10
}
resource "cloudflare_record" "ahoskingit_com_mx4" {
  zone_id  = cloudflare_zone.ahoskingit_com.id
  name     = cloudflare_zone.ahoskingit_com.zone
  type     = "MX"
  value    = "alt4.aspmx.l.google.com"
  priority = 10
}

resource "cloudflare_record" "ahoskingit_com_txt" {
  zone_id = cloudflare_zone.ahoskingit_com.id
  name    = cloudflare_zone.ahoskingit_com.zone
  type    = "TXT"
  ttl     = 120
  value   = "google-site-verification=8-QKnBQElI58gnygDWcCzvDGRh31c_bFmNMaSd0fCwE"
}

resource "cloudflare_record" "ahoskingit_house" {
  zone_id = cloudflare_zone.ahoskingit_com.id
  name    = "house.ahoskingit.com"
  type    = "CNAME"
  value   = join(".", [cloudflare_record.home_ahoskingit.name, cloudflare_zone.ahoskingit_com.zone])
  proxied = "true"
}
