resource "cloudflare_zone" "studi-o_com" {
  zone = "studi-odesigns.com"
}

resource "cloudflare_zone" "studi-o_ca" {
  zone = "studi-odesigns.ca"
}

resource "cloudflare_record" "studi-o_com_www" {
  zone_id = cloudflare_zone.studi-o_com.id
  name    = "www"
  value   = var.studi-odesigns_com
  type    = "CNAME"
  ttl     = 1
  proxied = true
}
resource "cloudflare_record" "studi-o_com" {
  zone_id = cloudflare_zone.studi-o_com.id
  name    = "studi-odesigns.com"
  value   = var.home_ip
  type    = "A"
  ttl     = 1
  proxied = true
}

resource "cloudflare_record" "studi-o_com_txt" {
  zone_id = cloudflare_zone.studi-o_com.id
  name    = "@"
  type    = "TXT"
  value   = "google-site-verification=AJw8umnraEhTYuHsrVc-5NvA7E2p08OF90HgPv8jWX4"
  ttl     = 120
}

resource "cloudflare_record" "studi-o_com_mx" {
  zone_id  = cloudflare_zone.studi-o_com.id
  name     = "@"
  type     = "MX"
  value    = "aspmx.l.google.com"
  priority = "1"
}

resource "cloudflare_record" "studi-o_com_mx1" {
  zone_id  = cloudflare_zone.studi-o_com.id
  name     = "@"
  type     = "MX"
  value    = "alt1.aspmx.l.google.com"
  priority = "5"
}

resource "cloudflare_record" "studi-o_com_mx2" {
  zone_id  = cloudflare_zone.studi-o_com.id
  name     = "@"
  type     = "MX"
  value    = "alt2.aspmx.l.google.com"
  priority = "5"
}
resource "cloudflare_record" "studi-o_com_mx3" {
  zone_id  = cloudflare_zone.studi-o_com.id
  name     = "@"
  type     = "MX"
  value    = "aspmx2.googlemail.com"
  priority = "10"
}
resource "cloudflare_record" "studi-o_com_mx4" {
  zone_id  = cloudflare_zone.studi-o_com.id
  name     = "@"
  type     = "MX"
  value    = "aspmx3.googlemail.com"
  priority = "10"
}

resource "cloudflare_record" "studi-o_ca_www" {
  zone_id = cloudflare_zone.studi-o_ca.id
  name    = "www"
  value   = "automatedbytes.com"
  type    = "CNAME"
  ttl     = 3600
}

resource "cloudflare_record" "studi-o_ca_txt" {
  zone_id = cloudflare_zone.studi-o_ca.id
  name    = "@"
  type    = "TXT"
  value   = "google-site-verification=1VQq6Qc0OSpPKs_cd9nKIAGiTw9I7uQbmwnlGd44yo8"
  ttl     = 120
}

resource "cloudflare_record" "studi-o_ca_mx" {
  zone_id  = cloudflare_zone.studi-o_ca.id
  name     = "@"
  type     = "MX"
  value    = "aspmx.l.google.com"
  priority = "1"
}

resource "cloudflare_record" "studi-o_ca_mx1" {
  zone_id  = cloudflare_zone.studi-o_ca.id
  name     = "@"
  type     = "MX"
  value    = "alt1.aspmx.l.google.com"
  priority = "5"
}

resource "cloudflare_record" "studi-o_ca_mx2" {
  zone_id  = cloudflare_zone.studi-o_ca.id
  name     = "@"
  type     = "MX"
  value    = "alt2.aspmx.l.google.com"
  priority = "5"
}
resource "cloudflare_record" "studi-o_ca_mx3" {
  zone_id  = cloudflare_zone.studi-o_ca.id
  name     = "@"
  type     = "MX"
  value    = "aspmx2.googlemail.com"
  priority = "10"
}
resource "cloudflare_record" "studi-o_ca_mx4" {
  zone_id  = cloudflare_zone.studi-o_ca.id
  name     = "@"
  type     = "MX"
  value    = "aspmx3.googlemail.com"
  priority = "10"
}
