# set these puppies on the command line with -var "variable=val"

## AWS credentials requirement
variable "aws_access_key" {}
variable "aws_secret_key" {}
variable "aws_region" {
  default = "us-east-1"
}

variable "home_ip" {
}

variable "site5_ip" {
  default = "138.197.154.98"
}

variable "do_assets" {
  default = "165.22.228.138"
}

## KFA IP
variable "kfa_ip" {
  default = "64.137.159.107"
}

variable "studi-odesigns_com" { default = "studi-odesigns.com" }
variable "studi-odesigns_ca" { default = "studi-odesigns.ca" }
variable "studi-odesign_com" { default = "studi-odesign.com" }
variable "studi-odesign_ca" { default = "studi-odesign.ca" }

## Vsphere creds - this did not work anywhere near as well as I'd hoped
#variable "vsphere_user" {}
#variable "vsphere_password" {}
#variable "vsphere_server" {}
variable "environment" {}


variable "ahosking_com_cnames" {
  default = ["code", "radarr", "sonarr", "ombi", "git",
    "monitors", "tv", "movies", "money", "cycles", "plex",
  "minecraft", "homeassistant", "slack", "status", "bills"]
}
