# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/cloudflare/cloudflare" {
  version     = "3.11.0"
  constraints = "~> 3.0"
  hashes = [
    "h1:oTTfUShNNcDCOxlXP2XiNleQYHmWswesQFCuicwQztw=",
    "h1:pb7gfJMtamHcmsNTAdB4V6iYWuXFLx3CErxqx2f+DXs=",
    "zh:0dd08c3336b3198e30158b61605674eefbc0b8e331ad8f25322ce1889fd2d8a7",
    "zh:196d80c7ae594f1b6140de02ecb101ec1afd7e45877be849ace73866f3fcb689",
    "zh:37bc087b5e858a92faf03c994cfbc4c906b0afecb7df2ff25961b394f3013cc0",
    "zh:3910c38a3f001879e67aede543ac1de44beab2249704fd016f51b14875815bed",
    "zh:6092e395636b673b8ee26dce9356331ac6ceaa6b62de17203dd151a22b9d9858",
    "zh:96167bd63b49df0d4921f30d81cda5162b03af2bd20a6c1da65ba15bd28a2d30",
    "zh:c168cab43707b4acdb8366074802df630cc4427a7c2e55c9489cdf56907d23fc",
    "zh:ccdde1cd64fbce75a9266e3df8a8f3dbd481cf72de53fa3a5fb15c78304843ea",
    "zh:db850c7627a312065867896c2bf0266b187beb24f3f898849c28364682f0646c",
    "zh:ddbd2d93f7a8ecd131b63a3336e5e1fed00258a9312c218f6fcf3e0f04733160",
    "zh:e8f02ec1dbf8dc0bcb4fcc29441fde52900f2182f88e1544074f8fb646ae89db",
    "zh:eef9d202238b76925e28fefd79621d4e5e9d3927cbcbce918222856300aa206b",
    "zh:f0ad67f42c4d8f3d20ca4e357ead759f651ac4bfbe0cd5006099deead8316e85",
    "zh:f1a2d6f4a26e193172b8c3a9411582ed3909df93b62314a1460b31d32a782bc6",
  ]
}

provider "registry.terraform.io/hashicorp/aws" {
  version     = "3.75.1"
  constraints = "~> 3.0"
  hashes = [
    "h1:++H0a4igODgreQL3SJuRz71JZkC69rl41R8xLYM894o=",
    "h1:OuaWibxL7IoGHmX6xuhO2XvYJcr+DJNr9pnVjaUiMrw=",
    "zh:11c2ee541ca1da923356c9225575ba294523d7b6af82d6171c912470ef0f90cd",
    "zh:19fe975993664252b4a2ff1079546f2b186b01d1a025a94a4f15c37e023806c5",
    "zh:442e7fc145b2debebe9279b283d07f5f736dc1776c2e5b1702728a6eb03789d0",
    "zh:7a77991b204ae2c16ac29a32226135d5fdbda40c8dafa77c5adf5439a346be77",
    "zh:89a257933181c15293c15a858fbfe7252129cc57cc2ec05b6c0b595d1bfe9d38",
    "zh:b1813ea5b6b0fd88ea85b1b21b8e4119566d1bc34feca297b4fb39d0536893cb",
    "zh:c519f3292ae431bd2381f88a95bd37c52f7a56d91feef88511e929344c180549",
    "zh:d3dbe88b661c073c174f04f73adc2720372143bdfa12f4fe8f411332e64662cf",
    "zh:e92a27e3c7295b031b5d62dd9428966c96e3157fc768b3d848a9ac60d1661c8e",
    "zh:ecd664c0d664fcf2d8a89a01462cb00bcae37da200305aef2de1b8fe185c9cd8",
    "zh:ed6ce1f9fa96aa28dd65842f852abed25f919d20b5cf53d26cec5b3f4d845725",
  ]
}
