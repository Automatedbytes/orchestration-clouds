terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "~> 3.0"
    }
  }
  required_version = ">= 1.8.2"
}

terraform {
  backend "remote" {
    organization = "AHosking"

    workspaces {
      name = "terraform"
    }
  }
}
variable "CLOUDFLARE_ACCOUNT_ID" {}
variable "CLOUDFLARE_EMAIL" {}
variable "CLOUDFLARE_API_KEY" {}
variable "CLOUDFLARE_API_TOKEN" {}

provider "cloudflare" {
  # email     = var.CLOUDFLARE_EMAIL
  # api_key   = var.CLOUDFLARE_API_KEY
  api_token = var.CLOUDFLARE_API_TOKEN
}

provider "aws" {
  region = "us-east-1"

  access_key = var.aws_access_key
  secret_key = var.aws_secret_key


  skip_credentials_validation = true
  skip_region_validation      = true
  skip_requesting_account_id  = true
  skip_metadata_api_check     = true
}
