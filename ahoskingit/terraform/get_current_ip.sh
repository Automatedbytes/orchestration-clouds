#!/bin/bash
DATE=`date`
OLD_IP=`cat /home/ahosking/projects/ahoskingit/orchestration.clouds/ahoskingit/terraform/homeip.auto.tfvars`
IP=`curl ifconfig.me`
echo 'home_ip = "'$IP'"' > homeip.auto.tfvars
MESSAGE="'$DATE' - Old IP was: '$OLD_IP' and the new IP is: '$IP'"
#echo $DATE

echo $MESSAGE
