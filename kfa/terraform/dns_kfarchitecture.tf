resource "cloudflare_zone" "kfarchitecture" {
  account_id = "af0161fbef7b405c91b39c3e32291efe"
  zone       = "kfarchitecture.com"
}

resource "cloudflare_record" "kfa" {
  zone_id = cloudflare_zone.kfarchitecture.id
  name    = "@"
  value   = "69.90.161.145"
  type    = "A"
  ttl     = 1
  proxied = false
}

resource "cloudflare_record" "kfa-vpn" {
  zone_id = cloudflare_zone.kfarchitecture.id
  name    = "vpn"
  value   = "64.137.159.107"
  type    = "A"
  ttl     = 1
  proxied = false
}

resource "cloudflare_record" "kfa-ftp" {
  zone_id = cloudflare_zone.kfarchitecture.id
  name    = "ftp"
  value   = "64.137.159.107"
  type    = "A"
  ttl     = 1
  proxied = true
}

resource "cloudflare_record" "kfa-support" {
  zone_id = cloudflare_zone.kfarchitecture.id
  name    = "support"
  value   = "99.240.201.161"
  type    = "A"
  ttl     = 1
  proxied = true
}

resource "cloudflare_record" "kfa-docs" {
  zone_id = cloudflare_zone.kfarchitecture.id
  name    = "docs.kfarchitecture.com"
  value   = "vpn.kfarchitecture.com"
  type    = "CNAME"
  ttl     = 1
  proxied = true
}

resource "cloudflare_record" "kfa-www" {
  zone_id = cloudflare_zone.kfarchitecture.id
  name    = "www.kfarchitecture.com"
  value   = "kfarchitecture.com"
  type    = "CNAME"
  ttl     = 1
  proxied = true
}


# email MX value for DNS
resource "cloudflare_record" "kfarchitecture_mx" {
  zone_id  = cloudflare_zone.kfarchitecture.id
  name     = cloudflare_zone.kfarchitecture.zone
  type     = "MX"
  ttl      = "60"
  value    = "aspmx.l.google.com"
  priority = 1
}
resource "cloudflare_record" "kfarchitecture_mx1" {
  zone_id  = cloudflare_zone.kfarchitecture.id
  name     = cloudflare_zone.kfarchitecture.zone
  type     = "MX"
  ttl      = "60"
  value    = "alt1.aspmx.l.google.com"
  priority = 5
}
resource "cloudflare_record" "kfarchitecture_mx2" {
  zone_id  = cloudflare_zone.kfarchitecture.id
  name     = cloudflare_zone.kfarchitecture.zone
  type     = "MX"
  ttl      = "60"
  value    = "alt2.aspmx.l.google.com"
  priority = 5
}
resource "cloudflare_record" "kfarchitecture_mx3" {
  zone_id  = cloudflare_zone.kfarchitecture.id
  name     = cloudflare_zone.kfarchitecture.zone
  type     = "MX"
  ttl      = "60"
  value    = "alt3.aspmx.l.google.com"
  priority = 10
}
resource "cloudflare_record" "kfarchitecture_mx4" {
  zone_id  = cloudflare_zone.kfarchitecture.id
  name     = cloudflare_zone.kfarchitecture.zone
  type     = "MX"
  ttl      = "60"
  value    = "alt4.aspmx.l.google.com"
  priority = 10
}
resource "cloudflare_record" "kfarchitecture_txt" {
  zone_id = cloudflare_zone.kfarchitecture.id
  name    = cloudflare_zone.kfarchitecture.zone
  type    = "TXT"
  ttl     = 60
  value   = "v=spf1 include:_spf.google.com ~all"
}
resource "cloudflare_record" "hostpapa_txt" {
  zone_id = cloudflare_zone.kfarchitecture.id
  name    = cloudflare_zone.kfarchitecture.zone
  type    = "TXT"
  ttl     = 60
  value   = "_globalsign-domain-verification=nwxV6NQSwZK6nkddpSLIPiZ7yZYff_yyum2p2SUz-a"
}
