terraform {
  required_providers {
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "~> 4.0"
    }
  }
  required_version = ">= 1.8.2"
}

terraform {
  backend "remote" {
    organization = "AHosking"

    workspaces {
      name = "kfa"
    }
  }
}

variable "CLOUDFLARE_API_TOKEN" {}

provider "cloudflare" {
  api_token = var.CLOUDFLARE_API_TOKEN
}
